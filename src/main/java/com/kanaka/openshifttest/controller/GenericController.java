package com.kanaka.openshifttest.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@RestController
public class GenericController {

    private String randomId = null;

    private static final Logger logger = LoggerFactory.getLogger(GenericController.class);

    @GetMapping
    public Map<Object, Object> testDefault() {
        Map result = new HashMap();
        result.put("status", "running");
        result.put("time", new Date());
        result.put("application id", randomId==null ? randomId = String.valueOf(UUID.randomUUID()) : randomId);
        logger.info("Time is {} ",new Date());
        logger.info("Application id is {} ",randomId);
        return result;
    }

    @GetMapping("{message}")
    public Map<Object, Object> test(@PathVariable String message) {
        Map result = new HashMap();
        result.put("status", "running");
        result.put("time", new Date());
        result.put("message", message);

        return result;
    }
}
