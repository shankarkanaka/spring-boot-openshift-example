package com.kanaka.openshifttest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OpenshifttestApplication {

	public static void main(String[] args) {
		SpringApplication.run(OpenshifttestApplication.class, args);
	}

}
